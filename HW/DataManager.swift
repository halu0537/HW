//
//  DataManager.swift
//  HW
//
//  Created by Халу on 07.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation

class DataManager {
    var textArray: [ModelText] = {
        var firstText = ModelText()
        firstText.myText = "ПЕРВАЯ НАДПИСЬ"
        firstText.type = "label"
        firstText.smallOrBig = false
        firstText.myTag = 0
        
        var secondText = ModelText()
        secondText.myText = "ВТОРАЯ НАДПИСЬ"
        secondText.type = "label"
        secondText.smallOrBig = false
        secondText.myTag = 1
        
        var thirdText = ModelText()
        thirdText.myText = "ТРЕТЬЯ НАДПИСЬ \n ТРЕТЬЯ НАДПИСЬ"
        thirdText.type = "textView"
        thirdText.height = 150.0
        thirdText.myTag = 2
        
        var fourthText = ModelText()
        fourthText.myText = "ЧЕТВЕРТАЯ НАДПИСЬ"
        fourthText.type = "textView"
        fourthText.height = 150.0
        fourthText.myTag = 3
        
        return [firstText, secondText, thirdText, fourthText]
    }()
}
