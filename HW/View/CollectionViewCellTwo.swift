//
//  CollectionViewCellTwo.swift
//  HW
//
//  Created by Халу on 06.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class CollectionViewCellTwo: UICollectionViewCell {
    
    @IBOutlet weak var textView: UITextView!
      
    var textViewText: ModelText? {
        didSet {
            textView.text = textViewText?.myText
        }
    }
}

