//
//  CollectionViewCellOne.swift
//  HW
//
//  Created by Халу on 06.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class CollectionViewCellOne: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!

    var labelText: ModelText? {
        didSet {
            label.text = labelText?.myText
        }
    }
}
