//
//  DisplayManager.swift
//  HW
//
//  Created by Халу on 07.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//
import UIKit

//протокол для обновления высоты CollectionView
protocol reloadCollectionViewDelegate {
    func reloadCollectionView()
}

class DisplayManager: NSObject  {
    var reloadDelegate: reloadCollectionViewDelegate?
    var dataManager = DataManager() // массив данных
}

extension DisplayManager: UICollectionViewDataSource {
    //количество ячеек
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataManager.textArray.count
    }
    //создание ячеек
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if dataManager.textArray[indexPath.row].type == "label" {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellOne", for: indexPath) as? CollectionViewCellOne{
                cell.labelText = dataManager.textArray[indexPath.row]
                cell.label.backgroundColor = UIColor.lightGray
                cell.label.font = UIFont.systemFont(ofSize: 20)
                
                return cell
            }
        } else {
            if dataManager.textArray[indexPath.row].type == "textView" {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellTwo", for: indexPath) as? CollectionViewCellTwo{
                    cell.textViewText = dataManager.textArray[indexPath.row]
                    cell.textView.backgroundColor = UIColor.lightGray
                    cell.textView.font = UIFont.systemFont(ofSize: 20)
                    cell.textView.tag = indexPath.row
                    return cell
                }
            }
        }
        return UICollectionViewCell()
    }
}

extension DisplayManager: UICollectionViewDelegate {
    // увеличить или уменьшить  lable
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if dataManager.textArray[indexPath.row].type == "label" {
            dataManager.textArray[indexPath.row].smallOrBig = !dataManager.textArray[indexPath.row].smallOrBig!
            print("\(dataManager.textArray[indexPath.row].myText!)")
            collectionView.reloadData()
        }
    }
}

extension DisplayManager: UICollectionViewDelegateFlowLayout {
    //размер ячеек
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if dataManager.textArray[indexPath.row].type == "textView"{
            return CGSize(width: ViewController.viewWidth!,
                                    height: dataManager.textArray[indexPath.row].height ?? 150.0)
        } else {
            let labelHeight: CGFloat = dataManager.textArray[indexPath.row].smallOrBig! ? 100 : 50
            return CGSize(width: ViewController.viewWidth!, height: labelHeight)
        }
    }
}


extension DisplayManager: UITextViewDelegate {
    //изменение размера TextView при наборе текста
    func textViewDidChange(_ textView: UITextView) {
        for text in dataManager.textArray {
            if text.myTag! == textView.tag {
                dataManager.textArray[textView.tag].myText = textView.text
                    if textView.contentSize.height >= 150.0 {
                        dataManager.textArray[textView.tag].height = textView.contentSize.height
                    }
                reloadDelegate?.reloadCollectionView()
            }
        }
    }
}
