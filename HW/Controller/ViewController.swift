//
//  ViewController.swift
//  HW
//
//  Created by Халу on 06.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//
import UIKit

class ViewController: UIViewController, reloadCollectionViewDelegate{
    
    static var viewWidth: CGFloat?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var displayManager: DisplayManager!
    
    //var displayManager = DisplayManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ViewController.viewWidth = view.bounds.width
        collectionView.dataSource = displayManager
        collectionView.delegate = displayManager
        displayManager.reloadDelegate = self
        
    }

    func reloadCollectionView() {
        //print("reloadCollectionView")
        collectionView.performBatchUpdates( _ :nil, completion:nil)
    }
}


