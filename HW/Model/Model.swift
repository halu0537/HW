//
//  Model.swift
//  HW
//
//  Created by Халу on 06.04.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation
import UIKit

struct ModelText {
    var myText: String?
    var type: String?
    var smallOrBig: Bool?
    var height: CGFloat?
    var myTag: Int?
}

